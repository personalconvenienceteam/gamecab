﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Low_Level_Objects_Library {
    /// <summary>
    /// Hand holds a collection of cards to represent a
    /// playing hand in a card game.
    /// 
    /// A hand can either be constructed as an empty hand
    /// or a list of cards can be passed to create a hand
    /// from those cards.
    /// 
    /// Roderick Lenz N9438157 2016
    /// </summary>
    public class Hand : IEnumerable {
        private List<Card> hand;

        /// <summary>
        /// Creates an empty hand
        /// </summary>
        public Hand() {
            hand = new List<Card>();
        }//end Hand

        /// <summary>
        /// Creates a hand from a list of cards 
        /// </summary>
        /// <param name="cards">a list of cards</param>
        public Hand(List<Card> cards) {
            hand = new List<Card>(cards);
        }//end Hand

        /// <summary>
        /// returns the number of cards in a hand
        /// </summary>
        /// <returns>number of cards as an integer</returns>
        public int GetCount() {
            return hand.Count();
        }//end GetCount

        /// <summary>
        /// Gets the card at the specified index in a hand
        /// </summary>
        /// <param name="index">index of card as an int</param>
        /// <returns>a card</returns>
        public Card GetCard(int index) {
            return hand[index];
        }//end GetCard

        /// <summary>
        /// Adds a specified card to a hand
        /// </summary>
        /// <param name="card">a card</param>
        public void Add(Card card) {
            hand.Add(card);
        }//end Add

        /// <summary>
        /// Checks a hand to see if it contains a specified card
        /// </summary>
        /// <param name="card">a card</param>
        /// <returns>true or false</returns>
        public bool Contains(Card card) {
            return hand.Contains(card);
        }//end Contains

        /// <summary>
        /// Removes the specified card from a hand if it exists
        /// returns true if successful
        /// </summary>
        /// <param name="card">a card</param>
        /// <returns>true or false</returns>
        public bool Remove(Card card) {
                return hand.Remove(card);
        }//end Remove

        /// <summary>
        /// Removes a card from a hand at the specified index
        /// </summary>
        /// <param name="index">an integer</param>
        public void RemoveAt(int index) {
                hand.RemoveAt(index);
        }//end RemoveAt

        /// <summary>
        /// Sorts the cards in a hand
        /// </summary>
        public void Sort() {
            hand.Sort();
        }//end Sort


        public IEnumerator GetEnumerator() {
            return hand.GetEnumerator();
        }// End GetEnumerator
    }// End class hand
}// End namespace
