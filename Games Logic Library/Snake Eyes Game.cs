﻿using Low_Level_Objects_Library;

namespace Games_Logic_Library {
    /// <summary>
    /// Snake_Eyes_Game handles the logic for a game of
    /// Snake Eyes.
    /// 
    /// I creates and stores the dice, handles rolls calculates
    /// scores and checks for round winner.
    /// 
    /// Roderick Lenz N9438157 2016
    /// </summary>
    public static class Snake_Eyes_Game {
        private const int NUMBER_OF_DICE = 2;
        enum winner { player, house, nowinner };

        private static int rollTotal, playerTotal, houseTotal, possiblePoints;
        private static Die[] dice;
        private static winner rollwon;

        /// <summary>
        /// SetUpGame is a constructor for a game of 
        /// Snake Eyes
        /// </summary>
        public static void SetUpGame() {
            rollTotal = 0;
            playerTotal = 0;
            houseTotal = 0;
            possiblePoints = 0;
            dice = new Die[NUMBER_OF_DICE] { new Die(), new Die() };
            rollwon = winner.nowinner;
        }// End SetUpGame

        
        /// <summary>
        /// FirstRoll handles the first roll of a round and returns
        /// true if a re-roll is required
        /// </summary>
        /// <returns></returns>
        public static bool FirstRoll() {

            // Roll the dice
            foreach (Die thisdie in dice) {
                thisdie.RollDie();
            }// end foreach
           
            rollTotal = GetRollTotal();

            // Check the roll total against possible outcomes
            switch (rollTotal) {
                case 2: playerTotal += 2; // Player wins 2 points if a 2 is rolled
                    rollwon = winner.player; 
                    return false;
                case 3:
                case 12: houseTotal += 2; // House wins 2 points if a 3 or 12 is rolled
                    rollwon = winner.house;
                    return false;
                case 7:
                case 11: playerTotal += 1; // Player wins 1 point for a 7 or 11
                    rollwon = winner.player;
                    return false;
                // the following values become the possible points and true is passed for a re-roll
                case 4:
                case 5:
                case 6:
                case 8:
                case 9:
                case 10: possiblePoints = rollTotal; 
                    rollwon = winner.nowinner;
                    return true;
            }// end switch

            return false;
        }// end FirstRoll


        /// <summary>
        /// AnotherRoll handles rolls for succesive rolls after the first
        /// If the roll matches the possible points, that number is added
        /// to the player score. 
        /// If a 7 is rolled the house wins 2 points.
        /// True is returned if neither 7 or the possible points value is 
        /// rolled indicating another roll is needed 
        /// </summary>
        /// <returns>true or false</returns>
        public static bool AnotherRoll() {
            int houseWinningNumber = 7;

            foreach (Die thisdie in dice) {
                thisdie.RollDie();
            }// end foreach

            rollTotal = GetRollTotal();

            if (rollTotal == possiblePoints) {
                playerTotal = playerTotal + possiblePoints;
                rollwon = winner.player;
                return false;
            } else if (rollTotal == houseWinningNumber) {
                houseTotal += 2;
                possiblePoints = 0;
                rollwon = winner.house;
                return false;
            }// end if else

            return true;
        }// end AnotherRoll


        /// <summary>
        /// an accessor method to get the die face value
        /// </summary>
        /// <param name="whichDie"></param>
        /// <returns>an integer</returns>
        public static int GetDiceFacevalue(int whichDie) {
            return dice[whichDie].GetFaceValue();
        }// end GetDiceFacevalue


        /// <summary>
        /// an accessor method for player points
        /// </summary>
        /// <returns>an integer</returns>
        public static int GetPlayersPoints() {
            return playerTotal;
        }// end GetPlayersPoints


        /// <summary>
        /// an accessor method for house points
        /// </summary>
        /// <returns>an integer</returns>
        public static int GetHousePoints() {
            return houseTotal;
        }// end GetHousePoints


        /// <summary>
        /// an accessor method for possible points
        /// </summary>
        /// <returns>an integer</returns>
        public static int GetPossiblePoints() {
            return possiblePoints;
        }// end GetPossiblePoints


        /// <summary>
        /// and accessor method that returns the last roll total
        /// </summary>
        /// <returns>an integer</returns>
        public static int GetRollTotal() {
            rollTotal = 0;

            foreach(Die thisDie in dice) {
                rollTotal = rollTotal + thisDie.GetFaceValue();
            }// end foreach

            return rollTotal;
        }// End GetRollTotal


        /// <summary>
        /// an accessor method that returns a string based on the
        /// winner of the most recent roll
        /// </summary>
        /// <returns>a string</returns>
        public static string GetRollOutcome() {
            switch (rollwon) {
                case winner.player: return "Congratulations,\n \tyou won!";
                case winner.house: return "The House Wins!";
                case winner.nowinner: return "Roll Again";
            }// End switch

            return "Roll Again";
        }// End GetRollOutcome
    }// End Snake_Eyes_Game class
}// End namespace
