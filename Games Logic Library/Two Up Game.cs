﻿using Low_Level_Objects_Library;

namespace Games_Logic_Library {
    /// <summary>
    /// Two_Up_Game handles the logic for a game of two
    /// up.
    /// 
    /// It creates a pair of coins and handles their tosses
    /// updating scores and returning toss results
    /// 
    /// Roderick Lenz N9438157 2016
    /// </summary>
    public static class Two_Up_Game {
        private static Coin coin1, coin2;
        private static int playersScore, computersScore;

        
        /// <summary>
        /// SetUpGame initializes the class variables and
        /// creates the coins
        /// </summary>
        public static void SetUpGame() {
            //Set scores to 0 on new game
            playersScore = 0;
            computersScore = 0;
            coin1 = new Coin();
            coin2 = new Coin();
        }// end SetUpGame

        
        /// <summary>
        /// TossCoins calls the Flip method in the coin class
        /// to randomize the faces of the coins
        /// </summary>
        public static void TossCoins() {
            //Toss coins
            coin1.Flip();
            coin2.Flip();
        }// end TossCoins


        /// <summary>
        /// TossOutcome updates scores and returns a string based on the state
        /// of the coins
        /// </summary>
        /// <returns>a string</returns>
        public static string TossOutcome() {
            string outcome = "";

            // if both coins are heads, player score is incremented
            if (coin1.IsHeads() && coin2.IsHeads()) {
                playersScore++;
                outcome = "Heads";

            //if both coins are tails, computer score is incremented
            } else if (!coin1.IsHeads() && !coin2.IsHeads()) {
                computersScore++;
                outcome = "Tails";
            
            // if both coins are different, no one scores
            } else if (coin1.IsHeads() != coin2.IsHeads()) {
                outcome = "Odds";
            } // end if-else

            return outcome;
        }// end TossOutcome


        /// <summary>
        /// IsHeads checks to see if a coin's value is head
        /// </summary>
        /// <param name="whichCoin">a coin</param>
        /// <returns>true or false</returns>
        public static bool IsHeads(int whichCoin) {
            bool result;

            result = whichCoin == 1 ? coin1.IsHeads() : coin2.IsHeads();

            return result;
        }// end IsHeads


        /// <summary>
        /// an accessor method for player score
        /// </summary>
        /// <returns>an integer</returns>
        public static int GetPlayersScore() {
            return playersScore;
        }// end GetPlayersScore

        
        /// <summary>
        /// and accessor method for computer score
        /// </summary>
        /// <returns>an integer</returns>
        public static int GetComputersScore() {
            return computersScore;
        }// end GetComputerScore
    }// End class Two_Up_Game
}// End namespace
