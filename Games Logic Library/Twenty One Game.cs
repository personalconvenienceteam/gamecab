﻿using Low_Level_Objects_Library;

namespace Games_Logic_Library {
    /// <summary>
    /// Twenty_One_Game handles the logic for a game of twenty one.
    /// It manages the hands and scores for two players.
    /// 
    /// Roderick Lenz N9438157 2016
    /// </summary>
    public static class Twenty_One_Game {
        private static CardPile cardPile;
        private static Hand[] hands;
        private static int[] totalPoints;
        private static int[] numOfGamesWon;
        private static int numOfUserAcesWithValueOne;


        /// <summary>
        /// SetupGame initializes the class variables
        /// </summary>
        public static void SetupGame() {
            const int NUMBER_OF_PLAYERS = 2;

            // Create and shuffle a new cardPile
            cardPile = new CardPile(true);
            cardPile.Shuffle();

            // Create two empty hands for the players
            hands = new Hand[NUMBER_OF_PLAYERS] { new Hand(), new Hand() };

            totalPoints = new int[NUMBER_OF_PLAYERS]; // Array to hold the scores of each player
            numOfGamesWon = new int[NUMBER_OF_PLAYERS]; // Array to hold the number of games won by each player

            numOfUserAcesWithValueOne = 0; // Used to adjust scores by counting number of aces valued as 1
        }// End Setup Game


        /// <summary>
        /// DealOneCardTo draws a card from the pile and 
        /// hands it to a hand, then returns that card
        /// to the calling method
        /// </summary>
        /// <param name="who">an integer</param>
        /// <returns>a card</returns>
        public static Card DealOneCardTo(int who) {
            Card dealtCard = cardPile.DealOneCard();
            hands[who].Add(dealtCard);
            return dealtCard;
        }// End DealOneCardTo

    
        /// <summary>
        /// CalcualteHandTotal calculates the points of
        /// the hand at the passed index and returns it
        /// adjusts score to account for 1 value aces if
        /// who is the player
        /// </summary>
        /// <param name="who">an integer</param>
        /// <returns>an integer</returns>
        public static int CalculateHandTotal(int who) {
            const int ACE_VALUE = 11; // Default points value of an ace

            int handTotal = 0;
            int cardValue = 0;
            
            foreach (Card card in hands[who]) {
                // Get the points value of the card as an int
                cardValue = GetCardPointsValue(card.GetFaceValue().ToString());

                // Add points value to handTotal
                handTotal += cardValue;
            }// End foreach

            // Update the players score in totalPoints
            totalPoints[who] = handTotal;

            // Adjust score for user's 1 value aces
            if (who == 0) {
                int scoreAdjustment = numOfUserAcesWithValueOne * ACE_VALUE;
                scoreAdjustment = scoreAdjustment - numOfUserAcesWithValueOne;
                totalPoints[who] -= scoreAdjustment;
            }// End if

            return handTotal;
        }// End CalculateHandTotal


        /// <summary>
        /// GetCardPointsValue is a helper method that stores the points
        /// values of cards as per the rules of the game.
        /// 
        /// A card's face value is passed to the method as a string and
        /// the appropriate points value is returned as an integer.
        /// </summary>
        /// <param name="cardValue">a string</param>
        /// <returns>an integer</returns>
        private static int GetCardPointsValue(string cardValue) {
            switch (cardValue) {
                case "Two": return 2;
                case "Three": return 3;
                case "Four": return 4;
                case "Five": return 5;
                case "Six": return 6;
                case "Seven": return 7;
                case "Eight": return 8;
                case "Nine": return 9;
                case "Ten":
                case "Jack":
                case "Queen":
                case "King": return 10;
                case "Ace": return 11;
            }
            return 0;
        }// End GetCardPointsVale


        /// <summary>
        /// CheckForWinner is called by PlayForDealer and examines the 
        /// scores to determine who the winner of a hand is and increments
        /// the approriate number of games in the numOfGamesWonArray
        /// </summary>
        private static void CheckForWinner() {
            const int PLAYER = 0, DEALER = 1;

            // If the player has under or equal to 21 but the Dealer is over, player wins
            if(totalPoints[PLAYER]<=21 && totalPoints[DEALER] > 21) {
                numOfGamesWon[PLAYER]++;
            // If the player is over 21, but the dealer is under or equal, dealer wins
            }else if(totalPoints[PLAYER] > 21 && totalPoints[DEALER] <= 21) {
                numOfGamesWon[DEALER]++;
            // If both are under the player who is closest wins
            }else if(totalPoints[PLAYER] <= 21 && totalPoints[DEALER] < 21) {
                if (totalPoints[PLAYER] > totalPoints[DEALER]) {
                    numOfGamesWon[PLAYER]++;
                }else if (totalPoints[PLAYER] < totalPoints[DEALER]) {
                    numOfGamesWon[DEALER]++;
                }// End if else
            }// End if else
        }// End CheckForWinner


        /// <summary>
        /// PlayForDealer is Called when the player goes bust or
        /// hits the stand button. If the player isn't bust the
        /// dealer draws until their score is greater than 17.
        /// CheckForWinner is called to update games won
        /// </summary>
        public static void PlayForDealer() {
            const int PLAYER = 0, DEALER = 1;
            if (totalPoints[PLAYER] <= 21) {
                while (totalPoints[DEALER] < 17) {
                    DealOneCardTo(DEALER);
                    CalculateHandTotal(DEALER);
                }// End while
            }// End if
            CheckForWinner();
        }// End PlayForDealer
    

        /// <summary>
        /// an accessor method for a hand
        /// </summary>
        /// <param name="who">an integer</param>
        /// <returns>a hand</returns>
        public static Hand GetHand(int who) {
            return hands[who];
        }// End GetHand


        /// <summary>
        /// an accessor method for totalPoints
        /// </summary>
        /// <param name="who">an integer</param>
        /// <returns>an integer</returns>
        public static int GetTotalPoints(int who) {
            return totalPoints[who];
        }// End GetTotalPoints


        /// <summary>
        /// an accessor method for numOfGamesWon
        /// </summary>
        /// <param name="who">an integer</param>
        /// <returns>an integer</returns>
        public static int GetNumOfGamesWon(int who) {
            return numOfGamesWon[who];
        }// End GetNumOfGamesWon

        
        /// <summary>
        /// an accessor method for numOfUserAcesWithValueOne
        /// </summary>
        /// <returns>an integer</returns>
        public static int GetNumOfUserAcesWithValueOne() {
            return numOfUserAcesWithValueOne;
        }// End GetNumOfUserAcesWithValueOne


        /// <summary>
        /// a mutator method for numOfUserAcesWithValueOne
        /// </summary>
        public static void IncrementNumOfUserAcesWithValueOne() {
            numOfUserAcesWithValueOne++;
        }// End IncrementNumOfUserAcesWithValueOne


        /// <summary>
        /// ResetTotals resets score totals, hands and creates a new
        /// card pile
        /// </summary>
        public static void ResetTotals() {
            const int NUMBER_OF_PLAYERS = 2;
            totalPoints = new int[NUMBER_OF_PLAYERS];

            numOfUserAcesWithValueOne = 0;

            cardPile = new CardPile(true);
            cardPile.Shuffle();

            hands = new Hand[NUMBER_OF_PLAYERS] { new Hand(), new Hand() };
        }// End ResetTotals
    }// End class Twenty_One_Game
}//End namespace
