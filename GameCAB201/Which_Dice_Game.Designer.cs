﻿namespace GameCAB201 {
    partial class Which_Dice_Game {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.whichGameLabel1 = new System.Windows.Forms.Label();
            this.diceGameComboBox = new System.Windows.Forms.ComboBox();
            this.exitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // whichGameLabel1
            // 
            this.whichGameLabel1.AutoSize = true;
            this.whichGameLabel1.Font = new System.Drawing.Font("Monotxt_IV25", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whichGameLabel1.Location = new System.Drawing.Point(40, 10);
            this.whichGameLabel1.Name = "whichGameLabel1";
            this.whichGameLabel1.Size = new System.Drawing.Size(220, 52);
            this.whichGameLabel1.TabIndex = 0;
            this.whichGameLabel1.Text = "Choose A Game\r\nTo Play";
            this.whichGameLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // diceGameComboBox
            // 
            this.diceGameComboBox.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diceGameComboBox.FormattingEnabled = true;
            this.diceGameComboBox.Items.AddRange(new object[] {
            "Snake Eyes",
            "Ship Captain and Crew"});
            this.diceGameComboBox.Location = new System.Drawing.Point(45, 79);
            this.diceGameComboBox.Name = "diceGameComboBox";
            this.diceGameComboBox.Size = new System.Drawing.Size(205, 22);
            this.diceGameComboBox.TabIndex = 1;
            this.diceGameComboBox.SelectedIndexChanged += new System.EventHandler(this.diceGameComboBox_SelectedIndexChanged);
            // 
            // exitButton
            // 
            this.exitButton.Font = new System.Drawing.Font("Monotxt_IV25", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(70, 180);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(151, 46);
            this.exitButton.TabIndex = 2;
            this.exitButton.Text = "EXIT";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // Which_Dice_Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 236);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.diceGameComboBox);
            this.Controls.Add(this.whichGameLabel1);
            this.Name = "Which_Dice_Game";
            this.Text = "Which_Dice_Game";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label whichGameLabel1;
        private System.Windows.Forms.ComboBox diceGameComboBox;
        private System.Windows.Forms.Button exitButton;
    }
}