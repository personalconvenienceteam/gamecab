﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Low_Level_Objects_Library;
using Games_Logic_Library;

namespace GameCAB201 {
    /// <summary>
    /// Twenty_One handles the GUI for a game of Twenty One
    /// 
    /// Roderick Lenz N9438157 2016
    /// </summary>
    public partial class Twenty_One : Form {
        private const int NUMBER_OF_PLAYERS = 2, PLAYER = 0, DEALER = 1;
        private TableLayoutPanel[] tableLayoutPanels;
        private Label[] bustedLabels;
        private Label[] pointsLabels;
        private Label[] gamesWonLabels;
        private Hand playerHand, dealerHand;

        
        /// <summary>
        /// Twenty_One() is the constructor for a game
        /// it initializes a game of Twenty One and class
        /// variables
        /// </summary>
        public Twenty_One() {
            InitializeComponent();

            Twenty_One_Game.SetupGame();

            tableLayoutPanels = new TableLayoutPanel[NUMBER_OF_PLAYERS] {playerHandTable, dealerHandTable};
            bustedLabels = new Label[NUMBER_OF_PLAYERS] {playerBustedLabel, dealerBustedLabel};
            pointsLabels = new Label[NUMBER_OF_PLAYERS] {playerScore, dealerScore};
            gamesWonLabels = new Label[NUMBER_OF_PLAYERS] {playerGamesWon, dealerGamesWon};
            UpdateScores();
        }// End Twenty_One

        
        /// <summary>
        /// DisplayGuiHand creates the picture boxes for the cards in
        /// a hand, retrieves the cards images and displays them in
        /// a table layout panel
        /// </summary>
        /// <param name="hand">a hand</param>
        /// <param name="tableLayoutPanel">the table layout panel for the hand</param>
        private void DisplayGuiHand(Hand hand, TableLayoutPanel tableLayoutPanel) {
            // Remove any cards already being shown.
            tableLayoutPanel.Controls.Clear();

            foreach (Card card in hand) {
                // Construct a PictureBox object.
                PictureBox pictureBox = new PictureBox();
            
                // Tell the PictureBox to use all the space inside its square.
                pictureBox.Dock = DockStyle.Fill;
                
                // Remove spacing around the PictureBox. (Default is 3 pixels.)
                pictureBox.Margin = new Padding(0);
                pictureBox.Image = Images.GetCardImage(card);
                
                // Add the PictureBox object to the tableLayoutPanel.
                tableLayoutPanel.Controls.Add(pictureBox);
            }//end foreach

        }// End DisplayGuiHand

        
        /// <summary>
        /// UpdateGame() is a helper method that calls the various
        /// methods to update elements of the game
        /// </summary>
        private void UpdateGame() {
            // Update scores in Twenty_One_Game
            Twenty_One_Game.CalculateHandTotal(PLAYER);
            Twenty_One_Game.CalculateHandTotal(DEALER);

            // Call update methods
            UpdateHands();
            UpdateGui();
            UpdateScores();
            CheckBusted();
        }// End UpdateGame

        
        /// <summary>
        /// CheckBusted() checks to see if either player has gone over 21
        /// and busted, if so it calls EndHand() to end the hand
        /// </summary>
        private void CheckBusted() {
            if (Twenty_One_Game.GetTotalPoints(PLAYER) > 21) {
                Twenty_One_Game.PlayForDealer();
                bustedLabels[PLAYER].Visible = true;
                UpdateScores();
                EndHand();
            }else if (Twenty_One_Game.GetTotalPoints(DEALER) > 21) {
                bustedLabels[DEALER].Visible = true;
                UpdateScores();
                EndHand();
            }// End if else
        }// End CheckBusted


        /// <summary>
        /// EndHand disables the hit and stand buttons, enables the deal
        /// button to allow the player to start a new hand and resets
        /// scores in Twenty_One_Game
        /// </summary>
        private void EndHand() {
            standButton.Enabled = false;
            hitButton.Enabled = false;
            dealButton.Enabled = true;

            Twenty_One_Game.ResetTotals();
        }// End EndHand

        
        /// <summary>
        /// CheckAce checks to see if a card is an Ace and displays
        /// a message box where appropriate
        /// </summary>
        /// <param name="dealtCard">a card</param>
        private void CheckAce(Card dealtCard) {
            const int Ace = 12;
            if ((int)dealtCard.GetFaceValue() == Ace) {
                ShowAceMessageBox();
            }
        }// End CheckAce

        
        /// <summary>
        /// Called by CheckAce, ShowAceMessageBox prompts the
        /// user when an ace is dealt and asks if they'd like
        /// that ace to counted as a one.
        /// 
        /// If the user indicates yes, the number of user aces
        /// in Twenty_One_Game is incremented and the score recalculated
        /// </summary>
        private void ShowAceMessageBox() {
            DialogResult result = MessageBox.Show("Count Ace as 1?",
                                  "Count Ace as 1?",
                                  MessageBoxButtons.YesNo,
                                  MessageBoxIcon.Question);

            if (result == DialogResult.Yes) {
                Twenty_One_Game.IncrementNumOfUserAcesWithValueOne();
            }
            Twenty_One_Game.CalculateHandTotal(PLAYER);
            UpdateScores();
        }// End ShowAceMessageBox


        /// <summary>
        /// UpdateHands gets the hands stored in Twenty_One_Game
        /// </summary>
        private void UpdateHands() {
            playerHand = Twenty_One_Game.GetHand(PLAYER);
            dealerHand = Twenty_One_Game.GetHand(DEALER);
        }// End UpdateHands

        
        /// <summary>
        /// UpdateGui passes the player and dealer hands to
        /// DisplayGuiHand
        /// </summary>
        private void UpdateGui() {
            DisplayGuiHand(playerHand, tableLayoutPanels[PLAYER]);
            DisplayGuiHand(dealerHand, tableLayoutPanels[DEALER]);
        }// End UpdateGUI

        
        /// <summary>
        /// UpdateScores retrieves the various score values
        /// from Twenty_One_Game and updates their respective
        /// GUI labels
        /// </summary>
        private void UpdateScores() {
            // Update points labels
            pointsLabels[PLAYER].Text = Twenty_One_Game.GetTotalPoints(PLAYER).ToString();
            pointsLabels[DEALER].Text = Twenty_One_Game.GetTotalPoints(DEALER).ToString();

            // Update games won labels
            gamesWonLabels[PLAYER].Text = Twenty_One_Game.GetNumOfGamesWon(PLAYER).ToString();
            gamesWonLabels[DEALER].Text = Twenty_One_Game.GetNumOfGamesWon(DEALER).ToString();

            // Update number of aces counted as one label
            numOfOneAces.Text = Twenty_One_Game.GetNumOfUserAcesWithValueOne().ToString();
        }// End UpdateScores

        
        /// <summary>
        /// dealButton_Click is the even handler for the deal button
        /// it resets any visible Busted labels from the previous game
        /// updates the scores to display 0s, deals two cards to each
        /// player and then disables the dealButton and enables the
        /// hit and stand buttons.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dealButton_Click(object sender, EventArgs e) {
            const int STARTING_CARDS = 2; // Constant for number of starting cards

            // Set busted labels visibility
            bustedLabels[PLAYER].Visible = false;
            bustedLabels[DEALER].Visible = false;

            // Update scores to show 0
            UpdateScores();

            for (int i = 0; i < STARTING_CARDS; i++) {
                // deal a card to the player
                Card dealtCard = Twenty_One_Game.DealOneCardTo(PLAYER);

                // get the hands and update the gui to display them
                UpdateHands();
                UpdateGui();

                // check to see if card dealt is an ace
                CheckAce(dealtCard);

                // update game board
                UpdateGame();

                // deal card to dealer
                Twenty_One_Game.DealOneCardTo(DEALER);
                UpdateGame();
            }// End for

            dealButton.Enabled = false;
            hitButton.Enabled = true;
            standButton.Enabled = true;
        }// End dealButton_Click


        /// <summary>
        /// Event handler for the stand button.
        /// 
        /// Calls PlayForDealer in Twenty_One_Game.
        /// Ends hand when dealer has finsihed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void standButton_Click(object sender, EventArgs e) {
            Twenty_One_Game.PlayForDealer();
            UpdateGame();
            EndHand();
        }// End standButton_Click

        
        /// <summary>
        /// Event handler for cancel button.
        /// Checks scores to see who won most games and displays 
        /// a message box with the result, then closes form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e) {
            if (Twenty_One_Game.GetNumOfGamesWon(PLAYER) > Twenty_One_Game.GetNumOfGamesWon(DEALER)) {
                MessageBox.Show("Congratulations!\n" +
                                "You Won");
            } else if (Twenty_One_Game.GetNumOfGamesWon(PLAYER) < Twenty_One_Game.GetNumOfGamesWon(DEALER)) {
                MessageBox.Show("The House Won");
            } else {
                MessageBox.Show("This match was a draw");
            }// End if else

            Close();
        }// End cancelButton_Click


        /// <summary>
        /// Event handler for hit button.
        /// Deals a single card to the player, displays it
        /// and checks if card is an ace, then updates the
        /// game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void hitButton_Click(object sender, EventArgs e) {
            Card dealtCard = Twenty_One_Game.DealOneCardTo(PLAYER);
            UpdateHands();
            UpdateGui();
            CheckAce(dealtCard);
            UpdateGame();

        }// End hitButton_Click


        /// <summary>
        /// Event handler for closing the form. Returns player to
        /// the Which_Card_Game form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Twenty_One_FormClosed(object sender, FormClosedEventArgs e) {
            Form gameForm = new Which_Card_Game();
            gameForm.Show();
        }// End Twenty_One_FormClosed

        
        /// <summary>
        /// Event Handler for test button used when developing
        /// Twenty_One_Game and Twenty_One.cs
        /// 
        /// otherwise unused
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void testButton_Click(object sender, EventArgs e) {
            const int testNumOfCardsForDealer = 2;
            const int testNumOfCardsForPlayer = 8;

            CardPile testCardPile = new CardPile(true);
            testCardPile.Shuffle();

            Hand testHandForDealer = new Hand(testCardPile.DealCards(testNumOfCardsForDealer));
            Hand testHandForPlayer = new Hand(testCardPile.DealCards(testNumOfCardsForPlayer));

            DisplayGuiHand(testHandForDealer, dealerHandTable);
            DisplayGuiHand(testHandForPlayer, playerHandTable);
        }// End testButton_Click
    }// End class Twenty_One
}// End namespace
