﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCAB201 {
    /// <summary>
    /// Unused class called for in brief part A
    /// 
    /// Roderick Lenz N9438157 2016
    /// </summary>
    public partial class Ship_Captain_Crew : Form {
        public Ship_Captain_Crew() {
            InitializeComponent();
        }

        private void Ship_Captain_Crew_FormClosed(object sender, FormClosedEventArgs e) {
            Form gameForm = new Which_Dice_Game();
            gameForm.Show();
        }
    }
}
