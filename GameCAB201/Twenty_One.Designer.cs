﻿namespace GameCAB201 {
    partial class Twenty_One {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dealerHandTable = new System.Windows.Forms.TableLayoutPanel();
            this.playerHandTable = new System.Windows.Forms.TableLayoutPanel();
            this.dealerBustedLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dealerScore = new System.Windows.Forms.Label();
            this.dealerGamesWon = new System.Windows.Forms.Label();
            this.playerBustedLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.playerScore = new System.Windows.Forms.Label();
            this.playerGamesWon = new System.Windows.Forms.Label();
            this.dealButton = new System.Windows.Forms.Button();
            this.hitButton = new System.Windows.Forms.Button();
            this.standButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.testButton = new System.Windows.Forms.Button();
            this.numOfOneAces = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dealerHandTable
            // 
            this.dealerHandTable.ColumnCount = 8;
            this.dealerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.dealerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.dealerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.dealerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.dealerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.dealerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.dealerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.dealerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.dealerHandTable.Location = new System.Drawing.Point(16, 40);
            this.dealerHandTable.Name = "dealerHandTable";
            this.dealerHandTable.RowCount = 1;
            this.dealerHandTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.dealerHandTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.dealerHandTable.Size = new System.Drawing.Size(576, 95);
            this.dealerHandTable.TabIndex = 0;
            // 
            // playerHandTable
            // 
            this.playerHandTable.ColumnCount = 8;
            this.playerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.playerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.playerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.playerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.playerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.playerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.playerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.playerHandTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.playerHandTable.Location = new System.Drawing.Point(16, 214);
            this.playerHandTable.Name = "playerHandTable";
            this.playerHandTable.RowCount = 1;
            this.playerHandTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.playerHandTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.playerHandTable.Size = new System.Drawing.Size(576, 95);
            this.playerHandTable.TabIndex = 1;
            // 
            // dealerBustedLabel
            // 
            this.dealerBustedLabel.AutoSize = true;
            this.dealerBustedLabel.Font = new System.Drawing.Font("Monotxt_IV25", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dealerBustedLabel.ForeColor = System.Drawing.Color.Red;
            this.dealerBustedLabel.Location = new System.Drawing.Point(31, 13);
            this.dealerBustedLabel.Name = "dealerBustedLabel";
            this.dealerBustedLabel.Size = new System.Drawing.Size(100, 24);
            this.dealerBustedLabel.TabIndex = 2;
            this.dealerBustedLabel.Text = "BUSTED";
            this.dealerBustedLabel.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Monotxt_IV25", 14.25F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(174, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "DEALER";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(450, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 14);
            this.label3.TabIndex = 4;
            this.label3.Text = "Games won";
            // 
            // dealerScore
            // 
            this.dealerScore.AutoSize = true;
            this.dealerScore.Font = new System.Drawing.Font("Monotxt_IV25", 14.25F, System.Drawing.FontStyle.Bold);
            this.dealerScore.Location = new System.Drawing.Point(309, 13);
            this.dealerScore.Name = "dealerScore";
            this.dealerScore.Size = new System.Drawing.Size(100, 24);
            this.dealerScore.TabIndex = 5;
            this.dealerScore.Text = "POINTS";
            // 
            // dealerGamesWon
            // 
            this.dealerGamesWon.AutoSize = true;
            this.dealerGamesWon.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Bold);
            this.dealerGamesWon.Location = new System.Drawing.Point(544, 20);
            this.dealerGamesWon.Name = "dealerGamesWon";
            this.dealerGamesWon.Size = new System.Drawing.Size(16, 14);
            this.dealerGamesWon.TabIndex = 6;
            this.dealerGamesWon.Text = "0";
            // 
            // playerBustedLabel
            // 
            this.playerBustedLabel.AutoSize = true;
            this.playerBustedLabel.Font = new System.Drawing.Font("Monotxt_IV25", 14.25F, System.Drawing.FontStyle.Bold);
            this.playerBustedLabel.ForeColor = System.Drawing.Color.Red;
            this.playerBustedLabel.Location = new System.Drawing.Point(31, 325);
            this.playerBustedLabel.Name = "playerBustedLabel";
            this.playerBustedLabel.Size = new System.Drawing.Size(100, 24);
            this.playerBustedLabel.TabIndex = 7;
            this.playerBustedLabel.Text = "BUSTED";
            this.playerBustedLabel.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Monotxt_IV25", 14.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(174, 325);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 24);
            this.label7.TabIndex = 8;
            this.label7.Text = "PLAYER";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(450, 325);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 14);
            this.label8.TabIndex = 9;
            this.label8.Text = "Games won";
            // 
            // playerScore
            // 
            this.playerScore.AutoSize = true;
            this.playerScore.Font = new System.Drawing.Font("Monotxt_IV25", 14.25F, System.Drawing.FontStyle.Bold);
            this.playerScore.Location = new System.Drawing.Point(309, 325);
            this.playerScore.Name = "playerScore";
            this.playerScore.Size = new System.Drawing.Size(100, 24);
            this.playerScore.TabIndex = 10;
            this.playerScore.Text = "POINTS";
            // 
            // playerGamesWon
            // 
            this.playerGamesWon.AutoSize = true;
            this.playerGamesWon.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Bold);
            this.playerGamesWon.Location = new System.Drawing.Point(544, 325);
            this.playerGamesWon.Name = "playerGamesWon";
            this.playerGamesWon.Size = new System.Drawing.Size(16, 14);
            this.playerGamesWon.TabIndex = 11;
            this.playerGamesWon.Text = "0";
            // 
            // dealButton
            // 
            this.dealButton.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dealButton.Location = new System.Drawing.Point(35, 375);
            this.dealButton.Name = "dealButton";
            this.dealButton.Size = new System.Drawing.Size(75, 23);
            this.dealButton.TabIndex = 12;
            this.dealButton.Text = "Deal";
            this.dealButton.UseVisualStyleBackColor = true;
            this.dealButton.Click += new System.EventHandler(this.dealButton_Click);
            // 
            // hitButton
            // 
            this.hitButton.Enabled = false;
            this.hitButton.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hitButton.Location = new System.Drawing.Point(145, 375);
            this.hitButton.Name = "hitButton";
            this.hitButton.Size = new System.Drawing.Size(75, 23);
            this.hitButton.TabIndex = 13;
            this.hitButton.Text = "Hit";
            this.hitButton.UseVisualStyleBackColor = true;
            this.hitButton.Click += new System.EventHandler(this.hitButton_Click);
            // 
            // standButton
            // 
            this.standButton.Enabled = false;
            this.standButton.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F);
            this.standButton.Location = new System.Drawing.Point(255, 375);
            this.standButton.Name = "standButton";
            this.standButton.Size = new System.Drawing.Size(75, 23);
            this.standButton.TabIndex = 14;
            this.standButton.Text = "Stand";
            this.standButton.UseVisualStyleBackColor = true;
            this.standButton.Click += new System.EventHandler(this.standButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F);
            this.cancelButton.Location = new System.Drawing.Point(365, 375);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 15;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // testButton
            // 
            this.testButton.Location = new System.Drawing.Point(517, 371);
            this.testButton.Name = "testButton";
            this.testButton.Size = new System.Drawing.Size(75, 23);
            this.testButton.TabIndex = 16;
            this.testButton.Text = "test";
            this.testButton.UseVisualStyleBackColor = true;
            this.testButton.Visible = false;
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // numOfOneAces
            // 
            this.numOfOneAces.AutoSize = true;
            this.numOfOneAces.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Bold);
            this.numOfOneAces.Location = new System.Drawing.Point(214, 353);
            this.numOfOneAces.Name = "numOfOneAces";
            this.numOfOneAces.Size = new System.Drawing.Size(16, 14);
            this.numOfOneAces.TabIndex = 17;
            this.numOfOneAces.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(236, 353);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 14);
            this.label4.TabIndex = 18;
            this.label4.Text = "Aces With Value of 1";
            // 
            // Twenty_One
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 421);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numOfOneAces);
            this.Controls.Add(this.testButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.standButton);
            this.Controls.Add(this.hitButton);
            this.Controls.Add(this.dealButton);
            this.Controls.Add(this.playerGamesWon);
            this.Controls.Add(this.playerScore);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.playerBustedLabel);
            this.Controls.Add(this.dealerGamesWon);
            this.Controls.Add(this.dealerScore);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dealerBustedLabel);
            this.Controls.Add(this.playerHandTable);
            this.Controls.Add(this.dealerHandTable);
            this.Name = "Twenty_One";
            this.Text = "Twenty One";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Twenty_One_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel dealerHandTable;
        private System.Windows.Forms.TableLayoutPanel playerHandTable;
        private System.Windows.Forms.Label dealerBustedLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label dealerScore;
        private System.Windows.Forms.Label dealerGamesWon;
        private System.Windows.Forms.Label playerBustedLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label playerScore;
        private System.Windows.Forms.Label playerGamesWon;
        private System.Windows.Forms.Button dealButton;
        private System.Windows.Forms.Button hitButton;
        private System.Windows.Forms.Button standButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button testButton;
        private System.Windows.Forms.Label numOfOneAces;
        private System.Windows.Forms.Label label4;
    }
}