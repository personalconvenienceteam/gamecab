﻿using System;
using System.Windows.Forms;

namespace GameCAB201 {
    /// <summary>
    /// Which_Card_Game is a form class that presents the user
    /// with a selection of card games to play
    /// 
    /// Roderick Lenz N9438157 2016
    /// </summary>
    public partial class Which_Card_Game : Form {
        /// <summary>
        /// A constructor
        /// </summary>
        public Which_Card_Game() {
            InitializeComponent();
        }// End Which_Card_Game

        /// <summary>
        /// Event handler for the cardGameComboBox starts selected
        /// game and closes Which_Card_Game form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cardGameComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            Form gameForm;

            string selectedGame = cardGameComboBox.SelectedItem.ToString();

            if (selectedGame == "Twenty-One") {
                gameForm = new Twenty_One();
            } else {
                gameForm = new Crazy_Eights();
            }// End if else

            Close();
            gameForm.Show();
        }// End cardGameComboBox_SelectedIndexChanged


        /// <summary>
        /// Event handler for when the exit button is clicked.
        /// Presents a confirmation dialog and then closes the
        /// form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitButton_Click(object sender, EventArgs e) {
            DialogResult result = MessageBox.Show("Would you like to quit and return to the main menu?",
                                        "Go back?",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);

            if (result == DialogResult.Yes) {

                Close();
            }// End if
        }// End exitButton_Click
    }// End class Which_Card_Game
}// End namespace