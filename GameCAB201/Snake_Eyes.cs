﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Games_Logic_Library;
using Low_Level_Objects_Library;

namespace GameCAB201 {
    /// <summary>
    /// Snake_Eyes is the Form class that handles
    /// the GUI for a game of Snake Eyes
    /// 
    /// Roderick Lenz N9438157
    /// </summary>
    public partial class Snake_Eyes : Form {
        private bool firstRoll;
        private int timerCounter;


        /// <summary>
        /// A constructor method for a game of snake eyes
        /// </summary>
        public Snake_Eyes() {
            InitializeComponent();
            Snake_Eyes_Game.SetUpGame();
            UpdateDiceImages();
            firstRoll = true;
        }// End Snake_Eyes


        /// <summary>
        /// UpdateDiceImages is a helper method called to
        /// retrieve the face values of the dice and pass
        /// them to UpdatePictureBoxImage
        /// </summary>
        private void UpdateDiceImages() {
            UpdatePictureBoxImage(pictureBox1, Snake_Eyes_Game.GetDiceFacevalue(0));
            UpdatePictureBoxImage(pictureBox2, Snake_Eyes_Game.GetDiceFacevalue(1));
        }// End UpdateDiceImages


        /// <summary>
        /// UpdatePictureBoxImage gets the die face image and displays in
        /// in the appropiate picture box
        /// </summary>
        /// <param name="whichPB">a PictureBox</param>
        /// <param name="faceValue">an integer</param>
        private void UpdatePictureBoxImage(PictureBox whichPB, int faceValue) {
            whichPB.Image = Images.GetDieImage(faceValue);
        }// end UpdatePictureBoxImage


        /// <summary>
        /// RollDice handles the main calls to Snake_Eyes_Game and the
        /// GUI updates.
        /// It checks to see which roll is being made, either a first roll
        /// or another roll and calls the appropriate method in Snake_Eyes_Game
        /// 
        /// It then updates labels, scores, images and buttons.
        /// </summary>
        private void RollDice() {
            bool rollAgain = true;

            // Check which roll needs to be made
            if (firstRoll) {
                rollAgain = Snake_Eyes_Game.FirstRoll();
                firstRoll = false;
            } else {
                rollAgain = Snake_Eyes_Game.AnotherRoll();
            }// End if else

            // Update GUI
            UpdateLabels();
            UpdateScores();
            UpdateDiceImages();

            // Test if roll again button needs to be enabled
            if (!rollAgain) {
                continueButton.Enabled = true;
                rollButton.Enabled = false;
            } else {
                rollButton.Enabled = true;
            }// End if else
        }// End RollDice


        /// <summary>
        /// UpdateLabels retrieves the outcome of the game
        /// and updates the label text
        /// </summary>
        private void UpdateLabels() {
            string outcomeText;

            // Get outcome text and pass to label1
            outcomeText = Snake_Eyes_Game.GetRollOutcome();
            label1.Text = outcomeText;

            // Test which text label 2 needs to show
            switch (outcomeText) {
                case "Roll Again":
                    label2.Text = "You need " + Snake_Eyes_Game.GetPossiblePoints() + " to win";
                    label2.Visible = true;
                    break;
                case "Congratulations,\n \tyou won!":
                    label2.Text = "You won";
                    label2.Visible = true;
                    break;
                case "The House Wins!":
                    label2.Text = "You lost";
                    label2.Visible = true;
                    break;
            }// end switch
        }// end UpdateLabels


        /// <summary>
        /// UpdateScores retrieves the scores from Snake_Eyes_Game and passes
        /// them to the score labels
        /// </summary>
        private void UpdateScores() {
            playerScore.Text = Snake_Eyes_Game.GetPlayersPoints().ToString();
            houseScore.Text = Snake_Eyes_Game.GetHousePoints().ToString();
        }// end UpdateScores


        /// <summary>
        /// Event handler for when the form is closed
        /// Re-opens the Which_Dice_Game form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Snake_Eyes_FormClosed(object sender, FormClosedEventArgs e) {
            Form gameForm = new Which_Dice_Game();
            gameForm.Show();
        }// End Snake_Eyes_FormClosed


        /// <summary>
        /// Event handler for the cancel button checks to see who
        /// has the greatest points and displays an appropriate message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e) {
            if (Snake_Eyes_Game.GetPlayersPoints() > Snake_Eyes_Game.GetHousePoints()) {
                MessageBox.Show("Congratulations, You won!");
            }else if (Snake_Eyes_Game.GetPlayersPoints() < Snake_Eyes_Game.GetHousePoints()) {
                MessageBox.Show("The House Won,\n" +
                                "better luck next time");
            }else {
                MessageBox.Show("This game was a draw");
            }

            Close();
        }// End cancelButton_Click


        /// <summary>
        /// Event handler for roll button. Disables the roll button
        /// sets the timer counter to 0 and starts timer1 for animation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rollButton_Click(object sender, EventArgs e) {
            rollButton.Enabled = false;
            timerCounter = 0;
            timer1.Start();
        }//end rollButton_Click


        /// <summary>
        /// Event handler for continue button, resets game to first roll
        /// keeping current scores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void continueButton_Click(object sender, EventArgs e) {
            rollButton.Enabled = true;
            firstRoll = true;
            continueButton.Enabled = false;
            label2.Visible = false;            
        }// End continueButton_Click


        /// <summary>
        /// Event handler for timer 1. Creates animation for first ten
        /// ticks and updates dice images.
        /// After 10th tick, stops the timer and calls Roll Dice to play
        /// the round
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e) {
            if (timerCounter < 10) {
                timerCounter++;
                // create dummy dice for roll
                Die animatedDie1 = new Die();
                Die animatedDie2 = new Die();

                // get dummy rolls
                animatedDie1.RollDie();
                animatedDie2.RollDie();

                // get dummy face values and display images
                UpdatePictureBoxImage(pictureBox1, animatedDie1.GetFaceValue());
                UpdatePictureBoxImage(pictureBox2, animatedDie2.GetFaceValue());
            }else {
                timer1.Stop();
                RollDice();
            }// End if else
        }// End timer1_Tick
    }// End class Snake_Eyes
}// End namespace
