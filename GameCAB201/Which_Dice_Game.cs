﻿using System;
using System.Windows.Forms;

namespace GameCAB201 {
    /// <summary>
    /// Which_Dice_Game is a form class that presents the user
    /// with a selection of dice games to play
    /// 
    /// Roderick Lenz N9438157 2016
    /// </summary>
    public partial class Which_Dice_Game : Form {
        /// <summary>
        /// A constructor
        /// </summary>
        public Which_Dice_Game() {
            InitializeComponent();
        } // End Which_Dice_Game

        
        /// <summary>
        /// Event handler for the diceGameComboBox starts selected
        /// game and closes Which_Dice_Game form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void diceGameComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            Form gameForm;

            string selectedGame = diceGameComboBox.SelectedItem.ToString();

            if (selectedGame == "Snake Eyes") {
                gameForm = new Snake_Eyes();
            } else {
                gameForm = new Ship_Captain_Crew();
            }// End if else

            Close();
            gameForm.Show();
        }// End diceGameComboBox_SelectedIndexChanged


        /// <summary>
        /// Event handler for when the exit button is clicked.
        /// Presents a confirmation dialog and then closes the
        /// form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitButton_Click(object sender, EventArgs e) {
            DialogResult result = MessageBox.Show("Would you like to quit and return to the main menu?",
                                                    "Go back?",
                                                     MessageBoxButtons.YesNo,
                                                     MessageBoxIcon.Question);

            if (result == DialogResult.Yes) {

                Close();
            }// End if
        }// End exitButton_Click
    }// End class Which_Dice_Game
}// End namespace