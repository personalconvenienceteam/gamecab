﻿using System;
using System.Windows.Forms;
using Games_Logic_Library;

namespace GameCAB201 {
    /// <summary>
    /// Two_Up handles the GUI for a game of Two Up
    /// 
    /// Roderick Lenz N9438157 2016
    /// </summary>
    public partial class Two_Up : Form {
        private int timerCounter;

        
        /// <summary>
        /// Constructor for the Two_Up form initializes
        /// the form, an instance of Two_Up_Game and
        /// sets up the GUI
        /// </summary>
        public Two_Up() {
            InitializeComponent();
            Two_Up_Game.SetUpGame();
            UpdateCoinImages();
            UpdateScore();
        }// End Two_Up

        
        /// <summary>
        /// UpdateCoinImages is a helper method that passes
        /// the coins to UpdatePictureBoxImage
        /// </summary>
        private void UpdateCoinImages() {
            UpdatePictureBoxImage(pictureBox1, Two_Up_Game.IsHeads(1));
            UpdatePictureBoxImage(pictureBox2, Two_Up_Game.IsHeads(2));
        }// End UpdateCoinImages


        /// <summary>
        /// UpdatePictureBoxImage gets the image for a coin and places
        /// it in the indicated picture box
        /// </summary>
        /// <param name="whichPB">a PictureBox</param>
        /// <param name="isHeads">a boolean value</param>
        private void UpdatePictureBoxImage(PictureBox whichPB, bool isHeads) {
            whichPB.Image = Images.GetCoinImage(isHeads);
        }// end UpdatePictureBoxImage


        /// <summary>
        /// Event handler for the cancel button, closes the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e) {
            Close();
        }// End cancelButton_Click


        /// <summary>
        /// Event handler for the play again button
        /// sets play again button to invisible and
        /// enables coin throw button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void playAgainButton_Click(object sender, EventArgs e) {
            playAgainButton.Visible = false;
            coinThrowButton.Enabled = true;
        }// End playAgainButton_Click


        /// <summary>
        /// Event handler for the coin throw button
        /// disables the coin throw button, starts timer1 
        /// and sets the timer counter to 0
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void coinThrowButton_Click(object sender, EventArgs e) {
            coinThrowButton.Enabled = false;
            timerCounter = 0;
            timer1.Start();
        }// End coinThrowButton_Click


        /// <summary>
        /// PlayGame calls the various method required
        /// for a single toss.
        /// Toss the coins, gets the game outcome and
        /// updates the labels then makes the play again
        /// button visible
        /// </summary>
        private void PlayGame() {
            string outcome;

            Two_Up_Game.TossCoins();
            outcome = Two_Up_Game.TossOutcome();

            winnerLabel.Visible = true;
            winnerLabel.Text = outcome;

            UpdateScore();
            UpdateCoinImages();

            playAgainButton.Visible = true;
        }// End PlayGame


        /// <summary>
        /// UpdateScore retrieves the scores from the Two_Up_Game
        /// class and updates the score labels
        /// </summary>
        private void UpdateScore() {
            int playScore, compScore;

            playScore = Two_Up_Game.GetPlayersScore();
            compScore = Two_Up_Game.GetComputersScore();

            playerScore.Text = playScore.ToString();
            computerScore.Text = compScore.ToString();
        }// End UpdateScore


        /// <summary>
        /// Event handler for the animation timer. On the first ten
        /// ticks the counter is incremented, the coins tossed and
        /// the images updated. On the next tick the timer stops
        /// and PlayGame is called
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e) {
            if (timerCounter < 10) {
                timerCounter++;
                Two_Up_Game.TossCoins();
                UpdateCoinImages();
            }else {
                timer1.Stop();
                PlayGame();
            }// End if else
        }// End timer1_Tick
    }// End class Two_Up
}// End namespace
