﻿namespace GameCAB201 {
    partial class Which_Card_Game {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.whichGameLabel1 = new System.Windows.Forms.Label();
            this.cardGameComboBox = new System.Windows.Forms.ComboBox();
            this.exitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // whichGameLabel1
            // 
            this.whichGameLabel1.AutoSize = true;
            this.whichGameLabel1.Font = new System.Drawing.Font("Monotxt_IV25", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whichGameLabel1.Location = new System.Drawing.Point(40, 10);
            this.whichGameLabel1.Name = "whichGameLabel1";
            this.whichGameLabel1.Size = new System.Drawing.Size(220, 52);
            this.whichGameLabel1.TabIndex = 1;
            this.whichGameLabel1.Text = "Choose A Game\r\nTo Play";
            this.whichGameLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cardGameComboBox
            // 
            this.cardGameComboBox.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardGameComboBox.FormattingEnabled = true;
            this.cardGameComboBox.Items.AddRange(new object[] {
            "Twenty-One",
            "Crazy Eights"});
            this.cardGameComboBox.Location = new System.Drawing.Point(45, 79);
            this.cardGameComboBox.Name = "cardGameComboBox";
            this.cardGameComboBox.Size = new System.Drawing.Size(205, 22);
            this.cardGameComboBox.TabIndex = 2;
            this.cardGameComboBox.SelectedIndexChanged += new System.EventHandler(this.cardGameComboBox_SelectedIndexChanged);
            // 
            // exitButton
            // 
            this.exitButton.Font = new System.Drawing.Font("Monotxt_IV25", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(70, 180);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(151, 46);
            this.exitButton.TabIndex = 3;
            this.exitButton.Text = "EXIT";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // Which_Card_Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 236);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.cardGameComboBox);
            this.Controls.Add(this.whichGameLabel1);
            this.Name = "Which_Card_Game";
            this.Text = "Which_Card_Game";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label whichGameLabel1;
        private System.Windows.Forms.ComboBox cardGameComboBox;
        private System.Windows.Forms.Button exitButton;
    }
}