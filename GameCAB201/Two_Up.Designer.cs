﻿namespace GameCAB201 {
    partial class Two_Up {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.coinThrowButton = new System.Windows.Forms.Button();
            this.playAgainButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.playerScore = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.computerScore = new System.Windows.Forms.Label();
            this.winnerLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(26, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(201, 20);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(150, 150);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // coinThrowButton
            // 
            this.coinThrowButton.Font = new System.Drawing.Font("Monotxt_IV25", 9.75F, System.Drawing.FontStyle.Bold);
            this.coinThrowButton.Location = new System.Drawing.Point(22, 253);
            this.coinThrowButton.Name = "coinThrowButton";
            this.coinThrowButton.Size = new System.Drawing.Size(117, 50);
            this.coinThrowButton.TabIndex = 2;
            this.coinThrowButton.Text = "Throw Coins";
            this.coinThrowButton.UseVisualStyleBackColor = true;
            this.coinThrowButton.Click += new System.EventHandler(this.coinThrowButton_Click);
            // 
            // playAgainButton
            // 
            this.playAgainButton.Font = new System.Drawing.Font("Monotxt_IV25", 9.75F, System.Drawing.FontStyle.Bold);
            this.playAgainButton.Location = new System.Drawing.Point(145, 253);
            this.playAgainButton.Name = "playAgainButton";
            this.playAgainButton.Size = new System.Drawing.Size(87, 50);
            this.playAgainButton.TabIndex = 3;
            this.playAgainButton.Text = "Play Again";
            this.playAgainButton.UseVisualStyleBackColor = true;
            this.playAgainButton.Visible = false;
            this.playAgainButton.Click += new System.EventHandler(this.playAgainButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Monotxt_IV25", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(238, 253);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(125, 50);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel Game";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 173);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Player\'s Score";
            // 
            // playerScore
            // 
            this.playerScore.AutoSize = true;
            this.playerScore.BackColor = System.Drawing.Color.White;
            this.playerScore.Location = new System.Drawing.Point(141, 173);
            this.playerScore.Name = "playerScore";
            this.playerScore.Size = new System.Drawing.Size(35, 13);
            this.playerScore.TabIndex = 6;
            this.playerScore.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(198, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Computer\'s Score";
            // 
            // computerScore
            // 
            this.computerScore.AutoSize = true;
            this.computerScore.BackColor = System.Drawing.Color.White;
            this.computerScore.Location = new System.Drawing.Point(316, 173);
            this.computerScore.Name = "computerScore";
            this.computerScore.Size = new System.Drawing.Size(35, 13);
            this.computerScore.TabIndex = 8;
            this.computerScore.Text = "label4";
            // 
            // winnerLabel
            // 
            this.winnerLabel.AutoSize = true;
            this.winnerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winnerLabel.Location = new System.Drawing.Point(141, 207);
            this.winnerLabel.Name = "winnerLabel";
            this.winnerLabel.Size = new System.Drawing.Size(51, 20);
            this.winnerLabel.TabIndex = 9;
            this.winnerLabel.Text = "label5";
            this.winnerLabel.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Two_Up
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 334);
            this.Controls.Add(this.winnerLabel);
            this.Controls.Add(this.computerScore);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.playerScore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.playAgainButton);
            this.Controls.Add(this.coinThrowButton);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Two_Up";
            this.Text = "Two_Up";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button coinThrowButton;
        private System.Windows.Forms.Button playAgainButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label playerScore;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label computerScore;
        private System.Windows.Forms.Label winnerLabel;
        private System.Windows.Forms.Timer timer1;
    }
}