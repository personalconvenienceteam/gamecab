﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCAB201 {
    public partial class Crazy_Eights : Form {
        public Crazy_Eights() {
            InitializeComponent();
        }

        private void Crazy_Eights_FormClosed(object sender, FormClosedEventArgs e) {
            Form gameForm = new Which_Card_Game();
            gameForm.Show();
        }
    }
}
