﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCAB201 {
    /// <summary>
    /// Form1 is the intial form for the GameCAB201 games cabinet.
    /// 
    /// It allows selection of 3 different game types: a coin game,
    /// dice games and card games.
    /// 
    /// Roderick Lenz N9438157 2016
    /// </summary>
    public partial class Form1 : Form {
        /// <summary>
        /// a constructor
        /// </summary>
        public Form1() {
            InitializeComponent();
        }// End Form1 


        /// <summary>
        /// Event handler for the game radioButtons.
        /// Enables start button when a game is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gameRadioButton_CheckedChanged(object sender, EventArgs e) {
            startButton.Enabled = true;
        }// End gameRadioButton_CheckedChanged


        /// <summary>
        /// Event handler for the start button.
        /// Opens the appropiate form based on the selected
        /// radio button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startButton_Click(object sender, EventArgs e) {
            Form gameForm;

            if (game1RadioButton.Checked) {
                gameForm = new Two_Up();
            } else if (game2RadioButton.Checked) {
                gameForm = new Which_Dice_Game();
            } else {
                gameForm = new Which_Card_Game();
            }

             gameForm.Show();
        }// End startButton_Click


        /// <summary>
        /// Event handler for exit button. Displays confirmation prompt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitButton_Click(object sender, EventArgs e) {
            DialogResult result = MessageBox.Show("Would you like to quit",
                                        "Quit?",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);

            if (result == DialogResult.Yes) {
                Close();
            }// End if
        }// End exitButton_Click
    }// End class Form1
}// End namespace
