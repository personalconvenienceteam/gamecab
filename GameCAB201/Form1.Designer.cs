﻿namespace GameCAB201 {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.gameLabel = new System.Windows.Forms.Label();
            this.gamesGroupBox1 = new System.Windows.Forms.GroupBox();
            this.game3RadioButton = new System.Windows.Forms.RadioButton();
            this.game2RadioButton = new System.Windows.Forms.RadioButton();
            this.game1RadioButton = new System.Windows.Forms.RadioButton();
            this.startButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.gamesGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gameLabel
            // 
            this.gameLabel.AutoSize = true;
            this.gameLabel.Font = new System.Drawing.Font("Monotxt_IV25", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameLabel.Location = new System.Drawing.Point(47, 36);
            this.gameLabel.Name = "gameLabel";
            this.gameLabel.Size = new System.Drawing.Size(204, 26);
            this.gameLabel.TabIndex = 0;
            this.gameLabel.Text = "GAME CAB 201";
            // 
            // gamesGroupBox1
            // 
            this.gamesGroupBox1.Controls.Add(this.game3RadioButton);
            this.gamesGroupBox1.Controls.Add(this.game2RadioButton);
            this.gamesGroupBox1.Controls.Add(this.game1RadioButton);
            this.gamesGroupBox1.Font = new System.Drawing.Font("Monotxt_IV25", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gamesGroupBox1.Location = new System.Drawing.Point(40, 76);
            this.gamesGroupBox1.Name = "gamesGroupBox1";
            this.gamesGroupBox1.Size = new System.Drawing.Size(220, 210);
            this.gamesGroupBox1.TabIndex = 1;
            this.gamesGroupBox1.TabStop = false;
            this.gamesGroupBox1.Text = "Choose A Game Type";
            // 
            // game3RadioButton
            // 
            this.game3RadioButton.AutoSize = true;
            this.game3RadioButton.Font = new System.Drawing.Font("Monotxt_IV25", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.game3RadioButton.Location = new System.Drawing.Point(36, 145);
            this.game3RadioButton.Name = "game3RadioButton";
            this.game3RadioButton.Size = new System.Drawing.Size(147, 24);
            this.game3RadioButton.TabIndex = 2;
            this.game3RadioButton.Text = "Card Games";
            this.game3RadioButton.UseVisualStyleBackColor = true;
            this.game3RadioButton.Click += new System.EventHandler(this.gameRadioButton_CheckedChanged);
            // 
            // game2RadioButton
            // 
            this.game2RadioButton.AutoSize = true;
            this.game2RadioButton.Font = new System.Drawing.Font("Monotxt_IV25", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.game2RadioButton.Location = new System.Drawing.Point(36, 95);
            this.game2RadioButton.Name = "game2RadioButton";
            this.game2RadioButton.Size = new System.Drawing.Size(147, 24);
            this.game2RadioButton.TabIndex = 1;
            this.game2RadioButton.Text = "Dice Games";
            this.game2RadioButton.UseVisualStyleBackColor = true;
            this.game2RadioButton.Click += new System.EventHandler(this.gameRadioButton_CheckedChanged);
            // 
            // game1RadioButton
            // 
            this.game1RadioButton.AutoSize = true;
            this.game1RadioButton.Font = new System.Drawing.Font("Monotxt_IV25", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.game1RadioButton.Location = new System.Drawing.Point(36, 45);
            this.game1RadioButton.Name = "game1RadioButton";
            this.game1RadioButton.Size = new System.Drawing.Size(135, 24);
            this.game1RadioButton.TabIndex = 0;
            this.game1RadioButton.Text = "Coin Game";
            this.game1RadioButton.UseVisualStyleBackColor = true;
            this.game1RadioButton.CheckedChanged += new System.EventHandler(this.gameRadioButton_CheckedChanged);
            // 
            // startButton
            // 
            this.startButton.Enabled = false;
            this.startButton.Font = new System.Drawing.Font("Monotxt_IV25", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButton.Location = new System.Drawing.Point(76, 313);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(150, 40);
            this.startButton.TabIndex = 2;
            this.startButton.Text = "START";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Font = new System.Drawing.Font("Monotxt_IV25", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(76, 375);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(150, 40);
            this.exitButton.TabIndex = 3;
            this.exitButton.Text = "EXIT";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 440);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.gamesGroupBox1);
            this.Controls.Add(this.gameLabel);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.gamesGroupBox1.ResumeLayout(false);
            this.gamesGroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label gameLabel;
        private System.Windows.Forms.GroupBox gamesGroupBox1;
        private System.Windows.Forms.RadioButton game3RadioButton;
        private System.Windows.Forms.RadioButton game2RadioButton;
        private System.Windows.Forms.RadioButton game1RadioButton;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button exitButton;
    }
}

